﻿using RPGLibrary;
using RPGLibrary.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGCharactersTests
{
    public class WarriorDpsTests
    {
        [Fact]
        public void LevelOneWarriorNoWeapon()
        {
            Warrior warrior = new Warrior("john");
            double expected = 1 * (1 + (5 / 100.0));
            double actual = warrior.Dps;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelOneWarriorWithAxe()
        {
            Warrior warrior = new Warrior("john");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior.EquipItem(testAxe);
            double expected = (7 * 1.1) * (1 + (5 / 100.0));
            double actual = warrior.Dps;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelOneWarriorWithAxeAndArmor()
        {
            Warrior warrior = new Warrior("john");
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                Attributes = { Vitality = 2, Strength = 1 }
            };
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100.0));
            double actual = warrior.Dps;

            Assert.Equal(expected, actual);
        }
    }
}
