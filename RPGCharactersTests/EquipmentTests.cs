﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGLibrary;
using RPGLibrary.CharacterClasses;
using RPGLibrary.Exceptions;
using Xunit;

namespace RPGCharactersTests
{
    public class EquipmentTests
    {
        [Fact]
        public void HighLevelWeaponShouldThrowInvalidWeaponException()
        {
            Weapon testAxe = new Weapon() 
            { 
                ItemName = "Common Axe", 
                ItemLevel = 2, 
                ItemSlot = ItemSlot.Weapon, 
                WeaponType = WeaponTypes.Axe, 
                WeaponAttributes = { Damage = 7, AttackSpeed = 1.1 }
            };
            Warrior warrior = new Warrior("john");

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
        }

        [Fact]
        public void HighLevelArmorShouldThrowInvalidArmorException()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                Attributes = { Vitality = 2, Strength = 1 }
            };
            Warrior warrior = new Warrior("john");

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }

        [Fact]
        public void InvalidWeaponTypeShouldThrowInvalidWeaponException()
        {
            Weapon testBow = new Weapon()
            {
                ItemName = "Common Bow",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Bow,
                WeaponAttributes = { Damage = 12, AttackSpeed = 0.8 }
            };
            Warrior warrior = new Warrior("john");

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
        }

        [Fact]
        public void InvalidArmorTypeShouldThrowInvalidArmorException()
        {
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Head,
                ArmorType = ArmorTypes.Cloth,
                Attributes = { Vitality = 1, Intelligence = 5 }
            };
            Warrior warrior = new Warrior("john");

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));
        }

        [Fact]
        public void SuccessfulWeaponEquipShouldReturnMessage()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = { Damage = 7, AttackSpeed = 1.1 }
            };
            Warrior warrior = new Warrior("john");

            string expected = "Common Axe equipped!";
            string actual = warrior.EquipItem(testAxe);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SuccessfulArmorEquipShouldReturnMessage()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = ItemSlot.Body,
                ArmorType = ArmorTypes.Plate,
                Attributes = { Vitality = 2, Strength = 1 }
            };
            Warrior warrior = new Warrior("john");

            string expected = "Common plate body armor equipped!";
            string actual = warrior.EquipItem(testPlateBody);

            Assert.Equal(expected, actual);
        }
    }
}
