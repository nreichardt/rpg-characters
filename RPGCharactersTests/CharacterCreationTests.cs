﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGLibrary;
using RPGLibrary.CharacterClasses;
using Xunit;

namespace RPGCharactersTests
{
    public class CharacterCreationTests
    {
        [Fact]
        public void CharacterIsLevelOneOnCreation()
        {
            Warrior warrior = new Warrior("john");
            int expected = 1;

            Assert.Equal(expected, warrior.Level);
        }

        [Fact]
        public void CharacterIsLevelTwoOnLevelUp()
        {
            Warrior warrior = new Warrior("john");
            warrior.LevelUp();
            int expected = 2;

            Assert.Equal(expected, warrior.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void ZeroOrNegativeLevelUpShouldThrowArgumentException(int LevelUpAmount)
        {
            //Arrange
            Warrior warrior = new Warrior("John");

            //Act and Assert
            Assert.Throws<ArgumentException>(() => warrior.LevelUp(LevelUpAmount));
        }

        [Fact]
        public void MageClassCreationAttributes()
        {
            //Arrange
            Mage mage = new Mage("John");
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };

            //Act and assert
            Assert.Equal(expected.Strength, mage.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, mage.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Vitality, mage.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Intelligence, mage.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void RangerClassCreationAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger("John");
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };

            //Act and assert
            Assert.Equal(expected.Strength, ranger.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, ranger.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Vitality, ranger.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Intelligence, ranger.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void RogueClassCreationAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue("John");
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };

            //Act and assert
            Assert.Equal(expected.Strength, rogue.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, rogue.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Vitality, rogue.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Intelligence, rogue.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void WarriorClassCreationAttributes()
        {

            Warrior warrior = new Warrior("John");
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };


            Assert.Equal(expected.Strength, warrior.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, warrior.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Vitality, warrior.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Intelligence, warrior.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void MageLevelTwoAttributes()
        {
            Mage mage = new Mage("John");
            mage.LevelUp(1);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13 };

            Assert.Equal(expected.Vitality, mage.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Strength, mage.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, mage.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Intelligence, mage.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void RangerLevelTwoAttributes()
        {
            Ranger ranger = new Ranger("John");
            ranger.LevelUp(1);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };

            Assert.Equal(expected.Vitality, ranger.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Strength, ranger.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, ranger.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Intelligence, ranger.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void RogueLevelTwoAttributes()
        {
            Rogue rogue = new Rogue("John");
            rogue.LevelUp(1);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 11, Strength = 3, Dexterity = 10, Intelligence = 2 };

            Assert.Equal(expected.Vitality, rogue.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Strength, rogue.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, rogue.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Intelligence, rogue.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void WarriorLevelTwoAttributes()
        {
            Warrior warrior = new Warrior("John");
            warrior.LevelUp(1);
            PrimaryAttributes expected = new PrimaryAttributes() { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };

            Assert.Equal(expected.Vitality, warrior.PrimaryAttributes.Vitality);
            Assert.Equal(expected.Strength, warrior.PrimaryAttributes.Strength);
            Assert.Equal(expected.Dexterity, warrior.PrimaryAttributes.Dexterity);
            Assert.Equal(expected.Intelligence, warrior.PrimaryAttributes.Intelligence);
        }

        [Fact]
        public void WarriorLevelTwoSecondaryAttributes()
        {
            Warrior warrior = new Warrior("John");
            warrior.LevelUp(1);
            SecondaryAttributes expected = new SecondaryAttributes() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };

            Assert.Equal(expected.Health, warrior.SecondaryAttributes.Health);
            Assert.Equal(expected.ArmorRating, warrior.SecondaryAttributes.ArmorRating);
            Assert.Equal(expected.ElementalResistance, warrior.SecondaryAttributes.ElementalResistance);
        }
    }
}
