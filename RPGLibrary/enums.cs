﻿namespace RPGLibrary
{
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public enum ItemSlot
    {
        Weapon,
        Head,
        Body,
        Legs
    }

}
