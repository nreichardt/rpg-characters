﻿namespace RPGLibrary
{
    public class Weapon : Item
    {
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; } = new WeaponAttributes();
    }
}
