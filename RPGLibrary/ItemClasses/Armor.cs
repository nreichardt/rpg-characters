﻿namespace RPGLibrary
{
    public class Armor : Item
    {
        public ArmorTypes ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; } = new PrimaryAttributes();
    }
}
