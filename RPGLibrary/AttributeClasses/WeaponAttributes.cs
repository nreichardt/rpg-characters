﻿namespace RPGLibrary
{
    public class WeaponAttributes
    {
        public int Damage { get; set; } = 1;
        public double AttackSpeed { get; set; } = 1;
    }
}
