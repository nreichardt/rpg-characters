﻿using RPGLibrary.Extensions;

namespace RPGLibrary.CharacterClasses
{
    public class Ranger : CharacterClass
    {
        public Ranger(string name) : base(name)
        {
            EquippableArmorTypes.AddMany(ArmorTypes.Leather, ArmorTypes.Mail);
            EquippableWeapons.Add(WeaponTypes.Bow);
            PrimaryAttributes.Vitality = 8;
            PrimaryAttributes.Strength = 1;
            PrimaryAttributes.Dexterity = 7;
            PrimaryAttributes.Intelligence = 1;
            CalculateAttributes();
        }

        protected override void IncreasePrimaryAttributes(int timesToIncrease)
        {
            PrimaryAttributes.Vitality += 2 * timesToIncrease;
            PrimaryAttributes.Strength += 1 * timesToIncrease;
            PrimaryAttributes.Dexterity += 5 * timesToIncrease;
            PrimaryAttributes.Intelligence += 1 * timesToIncrease;
        }

        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquippedItems[ItemSlot.Weapon] as Weapon;
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                     (1 + TotalPrimaryAttributes.Strength / 100.0);
        }
    }
}
