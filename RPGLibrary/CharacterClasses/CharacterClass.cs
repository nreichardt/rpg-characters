﻿using RPGLibrary.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGLibrary
{
    public abstract class CharacterClass
    {
        public string Name { get; protected set; }
        public int Level { get; protected set; } = 1;
        public double Dps { get; protected set; }
        public PrimaryAttributes PrimaryAttributes { get; protected set; } = new PrimaryAttributes();
        public PrimaryAttributes TotalPrimaryAttributes { get; protected set; }
        public SecondaryAttributes SecondaryAttributes { get; protected set; } = new SecondaryAttributes();
        public Dictionary<ItemSlot, Item> EquippedItems { get; protected set; } = new Dictionary<ItemSlot, Item>()
        {
            {
                ItemSlot.Head, new Armor()
            },
            {
                ItemSlot.Body, new Armor()
            },
            {
                ItemSlot.Legs, new Armor()
            },
            {
                ItemSlot.Weapon, new Weapon()
            }
        };
        public List<WeaponTypes> EquippableWeapons { get; protected set; } = new List<WeaponTypes>();
        public List<ArmorTypes> EquippableArmorTypes { get; protected set; } = new List<ArmorTypes>();

        public CharacterClass(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Abstract method for child classes to implement.
        /// Child classes should implement this so their level is increased by param value,
        /// should also call IncreasePrimaryAttributes() to increase primary stat according to times leveled up
        /// </summary>
        /// <param name="levelUpAmount"></param>
        public void LevelUp(int levelUpAmount = 1)
        {
            if (levelUpAmount < 1)
            {
                throw new ArgumentException("Invalid level up amount");
            }
            else
            {
                Level += levelUpAmount;
                IncreasePrimaryAttributes(levelUpAmount);
                CalculateAttributes();
            }
        }

        /// <summary>
        /// Abstract method for child classes to implement.
        /// Child classes should implement this to increase their primary values
        /// by their attribute growth amount, times the parameter value
        /// </summary>
        /// <param name="timesToIncrease"></param>
        protected abstract void IncreasePrimaryAttributes(int timesToIncrease);


        /// <summary>
        /// Calls the methods to calculate total primary attributes, and to calculate secondary attributes
        /// </summary>
        protected void CalculateAttributes()
        {
            CalculateTotalPrimaryAttributes();
            CalculateSecondaryAttributes();
            CalculateDps();
        }

        /// <summary>
        /// Calculates the characters total primary attributes, including attributes from armor. 
        /// </summary>
        protected void CalculateTotalPrimaryAttributes()
        {
            PrimaryAttributes tempPrimaryAttributes = new PrimaryAttributes();
            tempPrimaryAttributes += PrimaryAttributes;

            List<Armor> equippedAmors = EquippedItems.Where(x => x.Value.ItemSlot != ItemSlot.Weapon)
                .Select(x => x.Value as Armor).ToList();

            foreach (Armor equippedArmor in equippedAmors)
            {
                tempPrimaryAttributes += equippedArmor.Attributes;
            }

            TotalPrimaryAttributes = tempPrimaryAttributes;
        }

        /// <summary>
        /// Calculates the characters total secondary attributes, based on the characters total primary attributes
        /// </summary>
        protected void CalculateSecondaryAttributes()
        {
            SecondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + PrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;
        }

        /// <summary>
        /// Checks if a passed in weapon type and level requirement is equippable by the character class.
        /// If check returns true, the weapon is equipped.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>If weapon is equippable returns a string saying weapon is equipped.
        /// If weapon is not equippable an InvalidWeaponException is thrown</returns>
        public string EquipItem(Weapon weapon)
        {
            if (EquippableWeapons.Contains(weapon.WeaponType) && Level >= weapon.ItemLevel)
            {
                EquippedItems[ItemSlot.Weapon] = weapon;
                CalculateAttributes();
                return $"{weapon.ItemName} equipped!";
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        /// <summary>
        /// Checks if a passed in armor type and level requirement is equippable by the character class.
        /// If check returns true, the armor is equipped.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>If armor is equippable returns a string saying armor is equipped.
        /// If armor is not equippable an InvalidArmorException is thrown</returns>
        public string EquipItem(Armor armor)
        {
            if (EquippableArmorTypes.Contains(armor.ArmorType) && Level >= armor.ItemLevel)
            {
                EquippedItems[armor.ItemSlot] = armor;
                CalculateAttributes();
                return $"{armor.ItemName} equipped!";
            }
            else
            {
                throw new InvalidArmorException();
            }
        }

        /// <summary>
        /// Abstract method for child classes to implement to calculate dps using the class' primary dps attribute.
        /// </summary>
        public abstract void CalculateDps();


        /// <summary>
        /// Returns a stringbuilder containing all details and stats
        /// pertaining to the instance of the class
        /// </summary>
        /// <returns>Stringbuilder</returns>
        public StringBuilder PrintCharacterStats()
        {
            CalculateAttributes();
            StringBuilder characterStats = new StringBuilder("Character Stats:\n");
            characterStats.AppendLine();
            characterStats.AppendLine($"Name: {Name}");
            characterStats.AppendLine($"Level: {Level}");
            characterStats.AppendLine();
            characterStats.AppendLine("Primary Stats:");
            characterStats.AppendLine($"\tStrength: {TotalPrimaryAttributes.Strength}");
            characterStats.AppendLine($"\tDexterity: {TotalPrimaryAttributes.Dexterity}");
            characterStats.AppendLine($"\tIntelligence: {TotalPrimaryAttributes.Intelligence}");
            characterStats.AppendLine();
            characterStats.AppendLine("Secondary Stats:");
            characterStats.AppendLine($"\tHealth: {SecondaryAttributes.Health}");
            characterStats.AppendLine($"\tArmor Rating: {SecondaryAttributes.ArmorRating}");
            characterStats.AppendLine($"\tElemental Resistance: {SecondaryAttributes.ElementalResistance}");
            characterStats.AppendLine($"DPS: {Dps}");

            return characterStats;
        }
    }
}
