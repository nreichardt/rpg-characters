﻿using RPGLibrary.Extensions;

namespace RPGLibrary.CharacterClasses
{
    public class Warrior : CharacterClass
    {
        public Warrior(string name) : base(name)
        {
            EquippableArmorTypes.AddMany(ArmorTypes.Mail, ArmorTypes.Plate);
            EquippableWeapons.AddMany(WeaponTypes.Axe, WeaponTypes.Hammer, WeaponTypes.Sword);
            PrimaryAttributes.Vitality = 10;
            PrimaryAttributes.Strength = 5;
            PrimaryAttributes.Dexterity = 2;
            PrimaryAttributes.Intelligence = 1;
            CalculateAttributes();
        }

        protected override void IncreasePrimaryAttributes(int timesToIncrease)
        {
            PrimaryAttributes.Vitality += 5 * timesToIncrease;
            PrimaryAttributes.Strength += 3 * timesToIncrease;
            PrimaryAttributes.Dexterity += 2 * timesToIncrease;
            PrimaryAttributes.Intelligence += 1 * timesToIncrease;
        }

        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquippedItems[ItemSlot.Weapon] as Weapon;
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                     (1 + TotalPrimaryAttributes.Strength / 100.0);
        }
    }
}
