﻿using RPGLibrary.Extensions;

namespace RPGLibrary.CharacterClasses
{
    public class Mage : CharacterClass
    {
        public Mage(string name) : base(name)
        {
            EquippableArmorTypes.Add(ArmorTypes.Cloth);
            EquippableWeapons.AddMany(WeaponTypes.Staff, WeaponTypes.Wand);
            PrimaryAttributes.Vitality = 5;
            PrimaryAttributes.Strength = 1;
            PrimaryAttributes.Dexterity = 1;
            PrimaryAttributes.Intelligence = 8;
            CalculateAttributes();
        }

        protected override void IncreasePrimaryAttributes(int timesToIncrease)
        {
            PrimaryAttributes.Vitality += 3 * timesToIncrease;
            PrimaryAttributes.Strength += 1 * timesToIncrease;
            PrimaryAttributes.Dexterity += 1 * timesToIncrease;
            PrimaryAttributes.Intelligence += 5 * timesToIncrease;
        }
        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquippedItems[ItemSlot.Weapon] as Weapon;
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                     (1 + TotalPrimaryAttributes.Intelligence / 100.0);
        }
    }
}
