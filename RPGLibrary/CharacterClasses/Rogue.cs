﻿using RPGLibrary.Extensions;

namespace RPGLibrary.CharacterClasses
{
    public class Rogue : CharacterClass
    {
        public Rogue(string name) : base(name)
        {
            EquippableArmorTypes.AddMany(ArmorTypes.Leather, ArmorTypes.Mail);
            EquippableWeapons.AddMany(WeaponTypes.Dagger, WeaponTypes.Sword);
            PrimaryAttributes.Vitality = 8;
            PrimaryAttributes.Strength = 2;
            PrimaryAttributes.Dexterity = 6;
            PrimaryAttributes.Intelligence = 1;
            CalculateAttributes();
        }

        protected override void IncreasePrimaryAttributes(int timesToIncrease)
        {
            PrimaryAttributes.Vitality += 3 * timesToIncrease;
            PrimaryAttributes.Strength += 1 * timesToIncrease;
            PrimaryAttributes.Dexterity += 4 * timesToIncrease;
            PrimaryAttributes.Intelligence += 1 * timesToIncrease;
        }

        public override void CalculateDps()
        {
            Weapon equippedWeapon = EquippedItems[ItemSlot.Weapon] as Weapon;
            Dps = (equippedWeapon.WeaponAttributes.AttackSpeed * equippedWeapon.WeaponAttributes.Damage) *
                     (1 + TotalPrimaryAttributes.Dexterity / 100.0);
        }
    }
}
